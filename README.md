# easygo 轻松购

基于Flutter的跨平台电商应用

- Github地址：[easygo轻松购]()
- 包名：com.fluttertaste.easygo
- Flutter SDK 版本：3.0.0
- 默认支持平台：Android、iOS、Web、Linux、MacOS、Windows
- 开发适配平台：Android、iOS

### UI
- UI设计：[家居购物APP(作者：我要拒绝拖延症)](https://mastergo.com/community/resource/290?from=card)
- 字体：平方简


### 颜色说明
![image](asset/image/src/color.png)


### 规范
1. 资源（颜色、字符、style、图片）
2. widget

### 引用插件（版本见 pubspec.yaml）
- get: [路由和状态管理](https://pub.flutter-io.cn/packages/get)
- flutter_screenutil: [屏幕适配](https://pub.flutter-io.cn/packages/flutter_screenutil)
- shared_preferences: [sp缓存](https://pub.flutter-io.cn/packages/shared_preferences)
- image_picker: [图片选择](https://pub.flutter-io.cn/packages/image_picker)
- permission_handler: [权限管理](https://pub.flutter-io.cn/packages/permission_handler)
- dio: [网络请求](https://pub.flutter-io.cn/packages/dio)
- flutter_staggered_grid_view: [瀑布流](https://pub.flutter-io.cn/packages/flutter_staggered_grid_view)
- fluttertoast: [吐司](https://pub.flutter-io.cn/packages/fluttertoast)