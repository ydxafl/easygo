
import 'package:easygo/src/color.dart';
import 'package:flutter/material.dart';

class MSize{

  static const size22 = 22.0; /// 少量一级标题
  static const size18 = 18.0; /// 导航栏标题
  static const size16 = 16.0; /// 主层级标题、卡片标题
  static const size14 = 14.0; /// 主层级副标题、卡片副标题
  static const size13 = 13.0; /// 文本内容、说明文字
  static const size11 = 11.0; /// 标签文字、辅助说明

}



class MStyle{
  static const String fontPingFang = "pingfang";

  static const TextStyle bigTitle = TextStyle(color: Colors.black, fontSize: MSize.size22, fontWeight: FontWeight.bold);
  static const TextStyle homeBottom = TextStyle(color: ThemeColor.theme, fontSize: MSize.size11, fontWeight: FontWeight.bold);
  static const TextStyle homeBottomUnFocus = TextStyle(color: ThemeColor.theme, fontSize: MSize.size11, fontWeight: FontWeight.bold);
}

class MImage{
  /// 主页
  static const String homeNavigationMainFocus = "asset/image/icon/home_focus.png";
  static const String homeNavigationMainUnFocus = "asset/image/icon/home_unfocus.png";

  /// 社团
  static const String homeNavigationAssociationFocus = "asset/image/icon/association_focus.png";
  static const String homeNavigationAssociationUnFocus = "asset/image/icon/association_unfocus.png";

  /// 我的
  static const String homeNavigationMineFocus = "asset/image/icon/mine_focus.png";
  static const String homeNavigationMineUnFocus = "asset/image/icon/mine_unfocus.png";

  /// 消息
  static const String homeNavigationMessageFocus = "asset/image/icon/message_focus.png";
  static const String homeNavigationMessageUnFocus = "asset/image/icon/message_unfocus.png";

  static const String homeNavigationPublishUnFocus = "asset/image/icon/publish_close.png";
  static const String homeNavigationPublishFocus = "asset/image/icon/publish.png";

  static const String banner1 = "asset/image/src/banner_1.png";
  static const String banner2 = "asset/image/src/banner_2.png";
  static const String banner3 = "asset/image/src/banner_3.png";
  static const String banner4 = "asset/image/src/banner_4.png";
  static const String bannerbg = "asset/image/src/banner_bg.png";

  // 首页顶部icon
  static const String iconPhone = "asset/image/icon/phone.png";
  static const String iconGame = "asset/image/icon/game.png";
  static const String iconHousehold = "asset/image/icon/household.png";
  static const String iconWear = "asset/image/icon/wear.png";

  static const String indicatorTrue = "asset/image/icon/slider/choose.png";
  static const String indicatorFalse = "asset/image/icon/slider/unchoose.png";

  static const String home1 = "asset/image/card/首页推荐_买好物/ride.png";
  static const String home2 = "asset/image/card/首页推荐_买好物/dormitory.png";
  static const String home3 = "asset/image/card/首页推荐_买好物/gameLive.png";

  static const String head_4 = "asset/image/head/head_4.png";
  static const String head_9 = "asset/image/head/head_9.png";
  static const String head_25 = "asset/image/head/head_25.png";

  // header
  static const String follow = "asset/image/btn/follow.png";

  // sale
  static const String recovery = "asset/image/src/信用回收.png";
  static const String pricing = "asset/image/src/定价参考.png";

  // saleCard
  static const String saleOld1 = "asset/image/card/卖闲置_回收/旧衣 copy.png";
  static const String saleBook = "asset/image/card/卖闲置_回收/课本.png";
  static const String saleOld = "asset/image/card/卖闲置_回收/旧衣.png";
  static const String salePhone = "asset/image/card/卖闲置_回收/手机.png";

  static const String credit = 'asset/image/icon/credit.png';
  static const String realName = 'asset/image/icon/real_name.png';

}