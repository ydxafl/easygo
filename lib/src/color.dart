
import 'package:flutter/material.dart';

/// 颜色配置
class ThemeColor{

  /// 主题色
  static const MaterialColor theme = MaterialColor(
    0xFFFF5A5F,
    <int, Color>{
      50: Color(0xFFFF5A5F),
      100: Color(0xFFFF5A5F),
      200: Color(0xFFFF5A5F),
      300: Color(0xFFFF5A5F),
      400: Color(0xFFFF5A5F),
      500: Color(0xFFFF5A5F),
      600: Color(0xFFFF5A5F),
      800: Color(0xFFFF5A5F),
      900: Color(0xFFFF5A5F),
      700: Color(0xFFFF5A5F),
    },
  );

  static const MaterialColor themeLight = MaterialColor(
    0xFFFF888B,
    <int, Color>{
      500: Color(0xFFFF888B),
    },
  );

  static const MaterialColor themeGradientEnd = MaterialColor(
    0xFFFF3854,
    <int, Color>{
      500: Color(0xFFFF3854),
    },
  );

  static const MaterialColor themeMiddling = MaterialColor(
    0xFFFE4E53,
    <int, Color>{
      500: Color(0xFFFE4E53),
    },
  );

  static const MaterialColor themeDark = MaterialColor(
    0xFFFF3954,
    <int, Color>{
      500: Color(0xFFFF3954),
    },
  );


  /// 补充色
  static const MaterialColor supplement = MaterialColor(
    0xFFFF6C08,
    <int, Color>{
      500: Color(0xFFFF6C08),
    },
  );

  static const MaterialColor supplementLight = MaterialColor(
    0xFFFEEA67,
    <int, Color>{
      500: Color(0xFFFEEA67),
    },
  );

  static const MaterialColor supplementMiddling = MaterialColor(
    0xFFFECB47,
    <int, Color>{
      500: Color(0xFFFECB47),
    },
  );

  static const MaterialColor supplementDark = MaterialColor(
    0xFFFCA817,
    <int, Color>{
      500: Color(0xFFFCA817),
    },
  );

  /// 辅助色
  static const MaterialColor assist = MaterialColor(
    0xFF298DFF,
    <int, Color>{
      500: Color(0xFF298DFF),
    },
  );

  static const MaterialColor assistLight = MaterialColor(
    0xFFCFF5F8,
    <int, Color>{
      500: Color(0xFFCFF5F8),
    },
  );

  static const MaterialColor assistMiddling = MaterialColor(
    0xFF5EC8FE,
    <int, Color>{
      500: Color(0xFF5EC8FE),
    },
  );

  static const MaterialColor assistDark = MaterialColor(
    0xFF53A9FD,
    <int, Color>{
      500: Color(0xFF53A9FD),
    },
  );


  static const MaterialColor textColor = MaterialColor(
    0xFF666666,
    <int,Color>{
      500: Color(0xFF666666),
      valueDark: Color(0xFF000000),
      valueLight: Color(0xFF818181),
      valueWhite: Color(0xFFFFFFFF),
      valueGrade: Color(0xFFFF5A5F),
    },
  );

  static const valueDark = 0x01;
  static const valueLight = 0x02;
  static const valueWhite = 0x03;
  static const valueGrade = 0x04;

  static const MaterialColor alpha = MaterialColor(
    0x00ffffff,
    <int, Color>{
      500: Color(0x00ffffff),
    },
  );

  static const MaterialColor themeB = MaterialColor(
    0xFFFFE2E1,
    <int, Color>{
      500: Color(0xFFFFE2E1),
    },
  );

  static const MaterialColor textColorGray = MaterialColor(
    0xFFaaaaaa,
    <int, Color>{
      500: Color(0xFFAAAAAA),
    },
  );

}