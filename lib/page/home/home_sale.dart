
import 'package:easygo/page/widget/banner.dart';
import 'package:easygo/page/widget/home_bar_path.dart';
import 'package:easygo/page/widget/home_title.dart';
import 'package:easygo/src/color.dart';
import 'package:easygo/src/value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeSalePage extends StatelessWidget {
  const HomeSalePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Stack(
          children: [
            ClipPath(
              clipper: HomeBarPath(),
              child: Container(
                height: 120.h,
                decoration:  const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [ThemeColor.theme,ThemeColor.themeGradientEnd],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                ),
              ),
            ),
            BannerWidget(),
          ],
        ),

        Container(
          margin: EdgeInsets.only(top: 10.h),
          child: Row(
            mainAxisAlignment:MainAxisAlignment.center,
            children: [
              Expanded(child: Container()),

              /*Card(
                elevation: 10.r,
                child: Image.asset(MImage.recovery,fit: BoxFit.fill,width: 170.w,height: 130.h,),
              ),*/

              Image.asset(MImage.recovery,fit: BoxFit.fill,width: 170.w,height: 130.h,),

              Expanded(child: Container()),

              Image.asset(MImage.pricing,fit: BoxFit.fill,width: 170.w,height: 130.h,),

              Expanded(child: Container()),
            ],
          ),
        ),

        const HomeTitle(title: '   保价卖   ',),

        Container(
          height: 180.h,
          margin: EdgeInsets.only(top: 10.h),
          child: ListView.builder(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context,index){
              return Container(
                margin: EdgeInsets.only(left: 6.w),
                height: 180.h,
                width: 120.w,
                child: Column(
                  children: [
                    Image.asset(sale[index]["image"]!,fit: BoxFit.fill,width: 110.w,height: 150.h,),
                    Container(
                      margin: EdgeInsets.only(left: 6.w),
                      alignment: Alignment.centerLeft,
                      child: Text("${sale[index]["name"]}",style: TextStyle(fontWeight: FontWeight.bold),),
                    ),
                  ],
                ),
              );
            },
            itemCount: 4,
          ),
        ),

        const HomeTitle(title: '   去社团摆摊   ',),

        Container(
          margin: EdgeInsets.only(top: 10.h,left: 10.w,right: 10.w),
          child: GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context,index){
              return Container(
                margin: EdgeInsets.only(bottom: 6.h),
                width: 320.w/2,
                child: Card(
                  child: Column(
                    children: [
                      Image.asset(sale[index]["image"]!,fit: BoxFit.fill,width: 335.w/2,height: 180.h,),
                      Container(
                        margin: EdgeInsets.only(left: 6.w,top: 5.h),
                        alignment: Alignment.centerLeft,
                        child: Text("${sale[index]["name"]}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 13.sp),),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 6.w,top: 5.h),
                        alignment: Alignment.centerLeft,
                        child: Text("6.89w+人气  289件好物",style: TextStyle(fontSize: 10.sp),),
                      ),
                    ],
                  ),
                ),
              );
            },
            itemCount: 4,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing:10.w,
              mainAxisExtent:250.h,
            ),
          ),
        ),

      ],

    );
  }
}

var sale = [
  {"image":MImage.saleOld,"name":"旧衣"},
  {"image":MImage.saleOld1,"name":"数码速递"},
  {"image":MImage.salePhone,"name":"售价保卖"},
  {"image":MImage.saleBook,"name":"课本回收"},
];
