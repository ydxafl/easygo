
import 'dart:ui';

import 'package:easygo/controller/home_controller.dart';
import 'package:easygo/page/widget/banner.dart';
import 'package:easygo/page/widget/home_bar_path.dart';
import 'package:easygo/page/widget/home_title.dart';
import 'package:easygo/src/color.dart';
import 'package:easygo/src/value.dart';
import 'package:easygo/tools/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class HomeBuyPage extends StatelessWidget {

  final controller;
  HomeBuyPage({required HomeController this.controller,Key? key}) : super(key: key);
  final pageController = PageController(
    initialPage: 0,);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Stack(
          children: [
            ClipPath(
              clipper: HomeBarPath(),
              child: Container(
                height: 120.h,
                decoration:  const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [ThemeColor.theme,ThemeColor.themeGradientEnd],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                ),
              ),
            ),
            BannerWidget(),
          ],
        ),

        Container(
          margin: EdgeInsets.only(top: 5.h),
          height: 110.h,
          child: PageView(
            controller: pageController,
            onPageChanged: (index){
              controller.onIconChange(index);
            },
            children: [
              ListView(
                shrinkWrap: true,
                scrollDirection:Axis.horizontal,
                children: [
                  GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      width: 375.w/4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(icons[0]['image']!,height: 80.h,fit: BoxFit.fill,),
                          Text(icons[0]['name']!,textAlign: TextAlign.center),
                        ],
                      ),
                    ),
                    onTap: (){},
                  ),

                  GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      width: 375.w/4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(icons[1]['image']!,height: 80.h,fit: BoxFit.fill,),
                          Text(icons[1]['name']!,textAlign: TextAlign.center),
                        ],
                      ),
                    ),
                    onTap: (){},
                  ),

                  GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      width: 375.w/4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(icons[2]['image']!,height: 80.h,fit: BoxFit.fill,),
                          Text(icons[2]['name']!,textAlign: TextAlign.center),
                        ],
                      ),
                    ),
                    onTap: (){},
                  ),

                  GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      width: (375.w)/4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(icons[3]['image']!,height: 80.h,fit: BoxFit.fill,),
                          Text(icons[3]['name']!,textAlign: TextAlign.center),
                        ],
                      ),
                    ),
                    onTap: (){},
                  ),

                ],
              ),
              ListView.builder(
                shrinkWrap: true,
                scrollDirection:Axis.horizontal,
                itemBuilder: (context,index){
                  return GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      width: 375.w/4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(icons[index]['image']!,height: 80.h,fit: BoxFit.fill,),
                          Text(icons[index]['name']!,textAlign: TextAlign.center),
                        ],
                      ),
                    ),
                    onTap: (){

                    },
                  );
                },
                itemCount: 4,
              ),
            ],
          ),
        ),

        SizedBox(
          height: 8.h,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(() => controller.iconIndex.value[0]
                  ? Image.asset(MImage.indicatorTrue,)
                  : Image.asset(MImage.indicatorFalse,),),
              Container(width: 5.w,),
              Obx(() => controller.iconIndex.value[1]
                  ? Image.asset(MImage.indicatorTrue)
                  : Image.asset(MImage.indicatorFalse,),), // ,width: 20.w,fit: BoxFit.fill,
            ],
          ),
        ),

        const HomeTitle(title: '   专题推荐   ',),

        Container(
          margin: EdgeInsets.only(top: 20.h),
          height: 230.h,
          child: GridView(
            scrollDirection: Axis.horizontal,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                mainAxisExtent:375.w/3
            ),
            children: [
             Container(
               height: 200.h,
               width: 100.w,
               child:  Column(
                 mainAxisAlignment: MainAxisAlignment.start,
                 children: [
                   Card(
                     color: ThemeColor.alpha,
                     elevation: 10,
                     child: Container(
                       height: 150.h,
                       width: 100.w,
                       decoration: BoxDecoration(
                         image:  const DecorationImage(
                             image: AssetImage(
                               MImage.home1,),
                             fit: BoxFit.fill
                         ),
                         borderRadius: BorderRadius.circular(12),
                       ),
                     ),
                   ),
                   Container(
                     width: 100.w,
                     margin: EdgeInsets.only(top: 5.h),
                     alignment: Alignment.centerLeft,
                     child: Text('学习使人快乐',style: TextStyle(fontSize: 14.sp,fontWeight: FontWeight.bold),),
                   ),

                   Container(
                     width: 100.w,
                     margin: EdgeInsets.only(top: 5.h),
                     alignment: Alignment.centerLeft,
                     child: Text('2.1万人在逛',style: TextStyle(fontSize: 12.sp,color: Colors.grey),),
                   ),

                 ],
               ),
             ),
              Container(
                height: 200.h,
                width: 100.w,
                child:  Column(
                  children: [
                    Card(
                      color: ThemeColor.alpha,
                      elevation: 10,
                      child: Container(
                        height: 150.h,
                        width: 100.w,
                        decoration: BoxDecoration(
                          image:  const DecorationImage(
                              image: AssetImage(
                                MImage.home2,),
                              fit: BoxFit.fill
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    Container(
                      width: 100.w,
                      margin: EdgeInsets.only(top: 5.h),
                      alignment: Alignment.centerLeft,
                      child: Text('没想好穿搭',style: TextStyle(fontSize: 14.sp,fontWeight: FontWeight.bold),),
                    ),

                    Container(
                      width: 100.w,
                      margin: EdgeInsets.only(top: 5.h),
                      alignment: Alignment.centerLeft,

                      child: Text('2.1万人在逛',style: TextStyle(fontSize: 12.sp,color: Colors.grey),),
                    ),
                  ],
                ),
              ),
              Container(
                height: 200.h,
                width: 100.w,
                child:  Column(
                  children: [
                    Card(
                      color: ThemeColor.alpha,
                      elevation: 10,
                      child: Container(
                        height: 150.h,
                        width: 100.w,
                        decoration: BoxDecoration(
                          image:  const DecorationImage(
                              image: AssetImage(
                                MImage.home3,),
                              fit: BoxFit.fill
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    Container(
                      width: 100.w,
                      margin: EdgeInsets.only(top: 5.h),
                      alignment: Alignment.centerLeft,
                      child: Text('天使爱美丽',style: TextStyle(fontSize: 14.sp,fontWeight: FontWeight.bold),),
                    ),

                    Container(
                      width: 100.w,
                      margin: EdgeInsets.only(top: 5.h),
                      alignment: Alignment.centerLeft,
                      child: Text('2.1万人在逛',style: TextStyle(fontSize: 12.sp,color: Colors.grey),),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

        const HomeTitle(title: '   优质卖家推荐   ',),

        Container(
          height: 150.h,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection:Axis.horizontal,
            itemBuilder: (context,index){
              return GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  width: 375.w/4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Image.asset(header[index]['image']!,height: 60.h,fit: BoxFit.fill,),
                      Container(height: 5.h,),
                      Text(header[index]['name']!,textAlign: TextAlign.center,style: TextStyle(fontSize: 10.sp),),
                      Container(height: 5.h,),
                      GestureDetector(
                        child: Image.asset(header[index]['action']!,height: 20.h,fit: BoxFit.fill,),
                        onTap: (){
                          Toasts.show('关注了');
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){

                },
              );
            },
            itemCount: 4,
          ),
        ),

        const HomeTitle(title: '   去社区淘货   ',),

        Container(
          margin: EdgeInsets.only(top: 10.h),
          child: ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context,index){
              return const PostItem();
            },
            itemCount: 50,

          ),
        ),
      ],
    );
  }
}


class PostItem extends StatelessWidget {
  const PostItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.h),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 20.w),
            child:  Image.asset(MImage.head_25,height: 40.h,fit: BoxFit.fill,),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 10.w),
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:  [
                              Text('一只水蜜桃',style: TextStyle(fontSize: 15.sp,fontWeight: FontWeight.bold),),
                              Text('2小时前  北京',style: TextStyle(fontSize: 12.sp,),),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 20.w),
                          child: Text('￥199',style: TextStyle(fontSize: 18.sp,color: ThemeColor.theme,fontWeight: FontWeight.bold),),
                        ),
                      ],

                    ),
                  ),

                  SizedBox(height: 5.h,),

                  Container(
                    margin: EdgeInsets.only(right: 10.w),
                    alignment: Alignment.topLeft,
                    child:RichText(
                      text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <InlineSpan>[
                            WidgetSpan(
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.w),
                                  padding: const EdgeInsets.symmetric(horizontal: 2),
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.all(Radius.circular(20)),
                                      color: ThemeColor.themeB),
                                  child: Text(
                                    '九成新',
                                    style: TextStyle(color: ThemeColor.theme,fontSize: 10.sp),
                                  ),
                                )),
                            const TextSpan(text: '原素系列实木温莎椅 万年长青款 经典耐用的实木椅 折损率很小 且不占地 在宿舍用绝佳… '),

                          ]),
                    ),
                  ),

                  SizedBox(height: 5.h,),

                  Container(
                    height: 100.r,
                    margin: EdgeInsets.only(left: 5.w,right: 5.w),
                    child: ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index){
                        return Container(
                          height: 100.r,
                          width: 100.r,
                          decoration: BoxDecoration(
                            image:  const DecorationImage(
                                image: AssetImage(
                                  MImage.home1,),
                                fit: BoxFit.fill
                            ),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        );
                      },
                      itemCount: 5,
                    ),

                  ),

                  Container(
                    margin: EdgeInsets.only(top: 5.h),
                    child: Row(
                      children: [
                        const Expanded(child: Text('来自  能做不能站',style: TextStyle(color: ThemeColor.textColorGray),),),

                        Icon(Icons.remove_red_eye_outlined,size: 15.r),
                        const Text(' 139'),
                        Container(width: 20.w,),

                        Icon(Icons.mode_comment_outlined,size: 15.r,),
                        const Text(' 2'),
                        Container(width: 20.w,),

                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}



var icons = [
  {"image":MImage.iconPhone,"name":"手机数码"},
  {"image":MImage.iconGame,"name":"游戏交易"},
  {"image":MImage.iconHousehold,"name":"宿舍用品"},
  {"image":MImage.iconWear,"name":"穿搭好物"},
];


var header = [
  {"image":MImage.head_25,"name":"独角兽0625","action":MImage.follow},
  {"image":MImage.head_25,"name":"wachat","action":MImage.follow},
  {"image":MImage.head_25,"name":"小猪佩奇","action":MImage.follow},
  {"image":MImage.head_25,"name":"大猪乔治","action":MImage.follow},
];