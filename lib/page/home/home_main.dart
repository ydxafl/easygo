
import 'package:easygo/controller/home_controller.dart';
import 'package:easygo/page/home/home_buy.dart';
import 'package:easygo/page/home/home_sale.dart';
import 'package:easygo/page/widget/appbar.dart';
import 'package:easygo/page/widget/banner.dart';
import 'package:easygo/page/widget/home_bar_path.dart';
import 'package:easygo/route/route.dart';
import 'package:easygo/src/color.dart';
import 'package:easygo/tools/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class HomeMainPage extends StatelessWidget {
  HomeMainPage({Key? key}) : super(key: key);

  final controller = Get.put(HomeController());
  
  final pageController = PageController(
    initialPage: 0,);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(375.w,95.h),/// 240 还是 250
        child: Container(
          color: ThemeColor.theme,
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).padding.top,
                width: double.infinity,
              ),
              _select(context),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(() => GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(top: 4.h,bottom: 4.h,left: 30.w,right: 30.w),
                      height: 36.h,
                      alignment: Alignment.center,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("买好物",style: TextStyle(fontSize: 16.sp,color: Colors.white,fontWeight:controller.selete[0]?FontWeight.bold:FontWeight.normal),),
                          controller.selete[0]?Container(
                            margin: EdgeInsets.only(top: 3.h),
                            color: Colors.white,
                            width: 25.w,
                            height: 2.h,
                          ):Container(
                            margin: EdgeInsets.only(top: 3.h),
                            width: 25.w,
                            height: 2.h,
                          ),
                        ],
                      ),
                    ),
                    onTap: (){
                      controller.onSeleteChange(0);
                      pageController.animateToPage(0, duration: const Duration(microseconds: 300), curve: Curves.easeInOut);
                    },
                  )),
                  Obx(() => GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(top: 4.h,bottom: 4.h,left: 30.w,right: 30.w),
                      height: 36.h,
                      alignment: Alignment.center,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("卖闲置",style: TextStyle(fontSize: 16.sp,color: Colors.white,fontWeight:controller.selete[1]?FontWeight.bold:FontWeight.normal),),
                          controller.selete[1]?Container(
                            margin: EdgeInsets.only(top: 3.h),
                            color: Colors.white,
                            width: 25.w,
                            height: 2.h,
                          ):Container(
                            margin: EdgeInsets.only(top: 3.h),
                            width: 25.w,
                            height: 2.h,
                          ),
                        ],
                      ),
                    ),
                    onTap: (){
                      controller.onSeleteChange(1);
                      pageController.animateToPage(1, duration: const Duration(microseconds: 300), curve: Curves.easeInOut);
                    },
                  )),
                ],
              ),

            ],
          ),
        ),
      ),
      body: PageView(
        controller: pageController,
        onPageChanged: (index){
          controller.onSeleteChange(index);
        },
        children: [
          HomeBuyPage(controller :controller),
          HomeSalePage(),
        ],
      ),
    );
  }

  Widget _select(context) {
    return Container(
      margin: EdgeInsets.only(top: 10.h),
      height: 36.h,
      width: double.infinity,
      child:  Row(
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: const Center(
                child: Icon(Icons.date_range,color: Colors.white,),
              ),
            ),
          ),
          GestureDetector(
            child: Container(
              height: 32.h,
              width: 275.w,
              decoration: BoxDecoration(
                  color: ThemeColor.textColor[ThemeColor.valueWhite],
                  borderRadius: BorderRadius.circular(12.h)
              ),
              child:  Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(padding: const EdgeInsets.only(left: 10,right: 10),
                      child: Icon(Icons.search,color: Colors.black,size: 14.r,)),

                ],
              ),
            ),
            onTap: (){
              Toasts.show('点到了搜索栏，就不做');
            },
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: const Center(
                child: Icon(Icons.date_range,color: Colors.white,),
              ),
            ),
          ),
        ],
      ),
    );
  }

}
