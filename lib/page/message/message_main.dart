
import 'package:easygo/page/widget/appbar.dart';
import 'package:easygo/src/color.dart';
import 'package:flutter/material.dart';

class MessageMainPage extends StatelessWidget {
  const MessageMainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        flexibleSpace: AppBars.flexibleSpace,
        title: Text('123'),
      ),
      body: Center(
        child: ElevatedButton(onPressed: () {  }, child: Text('12'),),
      ),
    );
  }
}
