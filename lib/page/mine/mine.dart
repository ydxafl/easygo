
import 'package:easygo/page/widget/appbar.dart';
import 'package:easygo/page/widget/home_bar_path.dart';
import 'package:easygo/page/widget/mine_bar_path.dart';
import 'package:easygo/src/color.dart';
import 'package:easygo/src/value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MinePage extends StatelessWidget {
  const MinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('个人中心'),
        centerTitle: true,
        flexibleSpace: AppBars.flexibleSpace,
        backgroundColor: ThemeColor.theme,
      ),

      body: ListView(
        children: [
          Stack(
            children: [
              ClipPath(
                clipper: MineBarPath(),
                child: Container(
                  height: 120.h,
                  decoration:  const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [ThemeColor.theme,ThemeColor.themeGradientEnd],
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                    ),
                  ),
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 15.w,right: 15.w),
                child: Card(
                  elevation: 5,
                  child: Container(
                    height: 200.h,
                    width: double.infinity,
                    color: Colors.white,
                    child: Column(
                      children: [
                        Container(
                          height: 110.h,
                          width: double.infinity,
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [
                                    Container(
                                      margin: EdgeInsets.only(left: 20.w),
                                      alignment: Alignment.centerLeft,
                                      child: Text('独角兽0626',style: TextStyle(fontSize: 20.sp,fontWeight: FontWeight.bold),),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20.w,top: 5.h),
                                      alignment: Alignment.centerLeft,
                                      child: Text('蓝翔挖掘机技校   研二',style: TextStyle(fontSize: 14.sp,),),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20.w,top: 5.h),
                                      alignment: Alignment.centerLeft,
                                      child: Row(
                                        children: [
                                          Image.asset(MImage.credit,height: 15.h,),
                                          Image.asset(MImage.realName,height: 15.h,),
                                        ],
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(right: 10.w,bottom: 10.h),
                                width: 100.r,
                                height: 100.r,
                                child: Image.asset(MImage.head_4,width: 100.r,height: 100.r,),
                              ),
                            ],
                          ),
                        ),

                        Container(
                          height: 70.h,
                          width: double.infinity,
                          child: Text('123'),
                        ),

                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),

          Text('data'),
        ],
      ),

    );
  }
}
