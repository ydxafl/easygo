
import 'package:easygo/route/route.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IndexPage extends StatelessWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        centerTitle: true,
        title: Text('起始页'),
      ),

      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Get.offAllNamed(RouteName.home);
          },
          child: Text('开始'),
        ),
      ),
    );
  }
}
