
import 'package:easygo/controller/main_page_controller.dart';
import 'package:easygo/page/association/association.dart';
import 'package:easygo/page/home/home_main.dart';
import 'package:easygo/page/message/message_main.dart';
import 'package:easygo/page/mine/mine.dart';
import 'package:easygo/page/widget/home_bottom_icon.dart';
import 'package:easygo/src/value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  final controller = Get.put(MainPageController());
  final pages = [HomeMainPage(key: Key("HomeMainPage"),),const AssociationPage(key: Key("AssociationPage"),), const MessageMainPage(key: Key("MessageMainPage"),), const MinePage(key: Key("MinePage"),)];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(()=>IndexedStack(
        index:controller.currentIndex.value,
        children: pages,
      )),
      bottomNavigationBar:Container(
        height: 70.h,
        child:  BottomAppBar(
          elevation: 8.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                child: Obx(()=>HomeBottomIcon(
                  key: const Key("main"),
                  name: '首页',
                  onTap: (){
                    controller.onItemTapped(0);
                  },
                  path: controller.currentIndex.value == 0 ? MImage.homeNavigationMainFocus : MImage.homeNavigationMainUnFocus,
                )),
              ),
              Expanded(
                child: Obx(()=>HomeBottomIcon(
                  key: const Key("association"),
                  name: '社团',
                  onTap: (){
                    controller.onItemTapped(1);
                  },
                  path: controller.currentIndex.value == 1 ? MImage.homeNavigationAssociationFocus : MImage.homeNavigationAssociationUnFocus,
                )),
              ),
              Expanded(
                child: HomeBottomIcon(
                  key: const Key("publish"),
                  name: '发布',
                  onTap: (){
                  },
                  isIcon: true,
                  path: MImage.homeNavigationMessageFocus,
                ),
              ),
              Expanded(
                child: Obx(()=>HomeBottomIcon(
                  key: const Key("message"),
                  name: '信箱',
                  onTap: (){
                    controller.onItemTapped(2);
                  },
                  path: controller.currentIndex.value == 2 ? MImage.homeNavigationMessageFocus : MImage.homeNavigationMessageUnFocus,
                )),
              ),
              Expanded(
                child: Obx(()=>HomeBottomIcon(
                  key: const Key("mine"),
                  name: '我的',
                  onTap: (){
                    controller.onItemTapped(3);
                  },
                  path: controller.currentIndex.value == 3 ? MImage.homeNavigationMineFocus : MImage.homeNavigationMineUnFocus,
                )),
              ),

            ],
          ),
        ),
      ),

      floatingActionButton: Container(
        width: 60.w,
        height: 60.w,
        child: Image.asset(MImage.homeNavigationPublishFocus,fit: BoxFit.fill,),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

}
