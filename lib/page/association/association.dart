
import 'package:easygo/page/widget/appbar.dart';
import 'package:easygo/src/color.dart';
import 'package:flutter/material.dart';

class AssociationPage extends StatelessWidget {
  const AssociationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        flexibleSpace: AppBars.flexibleSpace,
      ),
      body: Container(
        color: ThemeColor.theme,
      ),
    );
  }
}
