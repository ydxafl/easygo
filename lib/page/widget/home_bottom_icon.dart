

import 'package:easygo/src/value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeBottomIcon extends StatelessWidget {
  const HomeBottomIcon({Key? key, required this.path,required this.onTap,required this.name,this.isIcon = false}) : super(key: key);
  final String path;
  final Function onTap;
  final String name;
  final bool isIcon;

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: isIcon?null:(){
        onTap();
      },
      child: Container(
        alignment: Alignment.center,
        height: 70.h,
        child: Column(
          mainAxisAlignment : MainAxisAlignment.center,
          children: [
            isIcon?SizedBox(width: 24.r,height: 24.r):Image.asset(path,width: 24.r,height: 24.r,),
            Text(name,style: MStyle.homeBottom,)
          ],
        ),
      ),

    );
  }
}
