
import 'package:flutter/material.dart';

class HomeBarPath extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {
    // TODO: implement getClip
    Path path = Path();
    path.moveTo(0,0);
    path.lineTo(0, (size.height)* 6/9);
    path.arcToPoint(Offset(size.width, (size.height)* 6/9),radius: Radius.circular(size.width*3/2));
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }


}