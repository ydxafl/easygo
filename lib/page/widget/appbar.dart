

import 'package:easygo/src/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppBars{

  static var appbar = AppBar(
    systemOverlayStyle: const SystemUiOverlayStyle(
      //在此处设置statusBarIconBrightness为全局设置
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light //light dark
    ),
    flexibleSpace: Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [ThemeColor.themeGradientEnd,ThemeColor.theme],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
    ),
  );

  static var flexibleSpace = Container(
    decoration: const BoxDecoration(
      gradient: LinearGradient(
        colors: [ThemeColor.theme,ThemeColor.themeGradientEnd],
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
      ),
    ),
  );

}
