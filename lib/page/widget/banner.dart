
import 'dart:async';

import 'package:easygo/controller/banner_controller.dart';
import 'package:easygo/controller/home_controller.dart';
import 'package:easygo/src/value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class Bannerss extends StatelessWidget {
  Bannerss({Key? key}) : super(key: key);

  final pageController = PageController(initialPage: 0,);
  final page = [const BannerImage(imageName: MImage.banner1,),const BannerImage(imageName: MImage.banner2),const BannerImage(imageName: MImage.banner3),const BannerImage(imageName: MImage.banner4)];

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        builder:  (controller){
          controller.onChange(pageController,page.length);
          return PageView.builder(
            controller: pageController,
            onPageChanged: (index){},
            itemBuilder: (BuildContext context, int index) {
              return page[ index ];
            },
            itemCount: page.length,
            /*children: const [
              BannerImage(imageName: MImage.banner1,),
              BannerImage(imageName: MImage.banner2,)
            ],*/
          );
        }
    );
  }
}

class Banners extends StatefulWidget {
  const Banners({Key? key}) : super(key: key);

  @override
  State<Banners> createState() => _BannersState();
}

class _BannersState extends State<Banners> {


  final page = [const BannerImage(imageName: MImage.banner1,),const BannerImage(imageName: MImage.banner2),const BannerImage(imageName: MImage.banner3),const BannerImage(imageName: MImage.banner4)];

  var timer;
  var pageController;
  var a= 0;

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      controller: pageController,
      onPageChanged: (index){
      },
      itemBuilder: (BuildContext context, int index) {
        return page[ index % 4];
      },
      itemCount: 1000,
    );
  }

  @override
  initState(){
    super.initState();
    pageController = PageController(initialPage: 0,);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      timer = Timer.periodic(Duration(microseconds: 3000), (timer) {
        a ++;
        pageController.animateToPage(a%2, duration: const Duration(microseconds: 300), curve: Curves.easeInOut);
        setState(() {});
      });
    });

  }

}


class BannerImage extends StatelessWidget {

  const BannerImage({Key? key,required this.imageName}) : super(key: key);
  final imageName;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      child: Image.asset(imageName,width: 351.w,height: 150.h,fit: BoxFit.fill,),
    );
  }
}

class BannerWidget extends StatelessWidget {
  BannerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      //height: 165.h,
      alignment: Alignment.bottomCenter,

      child: SizedBox(
        height: 150.h,
        width: double.infinity,
        child: Bannerss(),
      ),
    );
  }
}


