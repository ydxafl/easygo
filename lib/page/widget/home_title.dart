
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeTitle extends StatelessWidget {
  final String title;
  const HomeTitle({Key? key,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 20.w,
          height: 1.h,
          color: Colors.grey,
        ),
        Text(title,style: TextStyle(fontSize: 14.sp,fontWeight: FontWeight.bold),),
        Container(
          width: 20.w,
          height: 1.h,
          color: Colors.grey,
        ),
      ],
    );
  }
}
