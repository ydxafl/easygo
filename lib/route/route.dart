

import 'package:easygo/page/default/home.dart';
import 'package:easygo/page/default/index.dart';
import 'package:easygo/page/default/login.dart';
import 'package:get/get.dart';

class RouteName{
  static const String index = '/index';
  static const String home = '/home';
  static const String login = '/login';
}

var getPages = [
  GetPage(name: RouteName.index, page: () => const IndexPage()),
  GetPage(name: RouteName.home, page: () => HomePage()),
  GetPage(name: RouteName.login, page: () => const LoginPage()),
]; 