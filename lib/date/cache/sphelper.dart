import 'package:shared_preferences/shared_preferences.dart';

class UserCache{

  static Future<String?> getUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("user");
  }

  static Future<void> setUser(String value) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("user",value);
  }

}