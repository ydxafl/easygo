
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController{
  final _selete = [true,false].obs;
  get selete => _selete.value;

  final _iconIndex = [true,false].obs;
  get iconIndex => _iconIndex;

  onSeleteChange(index){
    if(index == 0){
      _selete.value = [true,false];
    }else{
      _selete.value = [false,true];
    }
  }


  void onChange(PageController controller, int length){
    var index = 0;
    Timer.periodic(const Duration(milliseconds: 3000), (timer) {
      index ++;
      try{
        if(index == length){
          controller.animateToPage(0, duration: const Duration(microseconds: 300), curve: Curves.easeInOut);
          index = 0;
        }else{
          controller.animateToPage(index, duration: const Duration(microseconds: 300), curve: Curves.easeInOut);
        }
      }catch(e){
        //debugPrint('try:$a');
      }
    });

  }

  onIconChange(index){
    if(index == 0){
      _iconIndex.value = [true,false];
    }else{
      _iconIndex.value = [false,true];
    }
  }


}